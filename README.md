# scBCR

This repository is intended to install software to perform BCR reconstrucion. **Post-processing analysis of the obtained BCR to compute the sensitivity and accuracy of each method can be obtained using [this "scBCR" github repository](https://github.com/tAndreani/scBCR)**.

The docker image contains software suite for **immunologic Single Cell analysis of B cell receptors and antibodies repertoires reconstruction**. These tools can be used to **reconstruct paired heavy + light chains** antibodies with single cell plate-based sequencing methods data coming from SMART-seq2 and drop-seq data coming from 10x 5' or VDJ chemistry. At the moment, in the literature, there are available seven methods but for the purpose of this benchmark we decided to evaluate the following ones:

* [BraCeR ](https://github.com/Teichlab/bracer) commit  131c2b9
* [BALDR](https://github.com/BosingerLab/BALDR) commit 461d9b0
* [vdjpuzzle2](https://bitbucket.org/kirbyvisp/vdjpuzzle2/src/master/) v3.0
* [BASIC](https://people.cs.uchicago.edu/~aakhan/BASIC/) v1.5.0
* [MIXCR](https://github.com/milaboratory/mixcr) v3.0.13
* [TRUST4](https://github.com/liulab-dfci/TRUST4) v1.0.4

As all these tools have same requirements but in different versions (python, pysam, igblastn, ...), they have to be called in **specific environment**.
These environments are described bellow.

At the moment we have tested this repository and run all the test software on:

1) Linux Centos 7.4.1708 - Succeeded - check the output [here](https://drive.google.com/drive/folders/1Bpu5Qdz6X0VbrD371ey1jr_XD_Pd1A1Y?usp=sharing)
2) Ubuntu 20.04.2 LTS - Succeeded - check the output [here](https://drive.google.com/drive/folders/1G5BoEcaHNc2RTgfHu_RIOibN4pPYxu5K?usp=sharing)
3) MacBook Pro OS Sierra (Retina, 13-inch, Early 2015, Processor 2.7 GHz Dual-Core Intel Core i5, Memory 8 GB 1867 MHz DDR3) - Failed

To build the docker image please do: 

```
wget https://gitlab.com/tAndreani/scBCR/-/archive/master/scBCR-master.zip && unzip scBCR-master.zip
cd scBCR-master/
sudo docker build . --build-arg http_proxy=${http_proxy} --build-arg https_proxy=${https_proxy} -t scbcr:0.1 &> log.txt
```

Please feel free to reach out if you want to test in your machine. Feedback are welcome.

## Post installation command
```
wget http://ftp.ensembl.org/pub/release-89/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz;
wget http://ftp.ensembl.org/pub/release-89/gtf/homo_sapiens/Homo_sapiens.GRCh38.89.gtf.gz;
cd /GENOMES/STAR_GRCh38;
gunzip Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz;
gunzip Homo_sapiens.GRCh38.89.gtf.gz;

cd /GENOMES/BOWTIE2_INDEXES;
bowtie2-build /GENOMES/STAR_GRCh38/Homo_sapiens.GRCh38.dna.primary_assembly.fa homo_sapiens_GRCh38;
cp /GENOMES/STAR_GRCh38/Homo_sapiens.GRCh38.dna.primary_assembly.fa /GENOMES/BOWTIE2_INDEXES/homo_sapiens_GRCh38.fa
export BOWTIE2_INDEXES=/GENOMES/BOWTIE2_INDEXES
```
## Native environment

Natively, scBCR image can launch `BRACER`, `MIXCR` and `TRUST4`.

Bracer input files must be copied in the workdir specified in the command line.

```
cd /opt/sample/bracer
cp 04.shm15.1.fq /root/workdir
cp 04.shm15.2.fq /root/workdir
cd /root/workdir
bracer assemble -p 2 -s Hsap test /root/workdir 04.shm15.1.fq 04.shm15.2.fq
```

## omics conda environment

This environment is used to launch `BALDR` and `BRAPES`.
```
source /opt/anaconda/anaconda3/etc/profile.d/conda.sh
conda activate omics
```
**note :** Trinity output file name does not match Igblastn input file. **Changes has been made in BALDR source code** to create a symbolic link from Trinity output that fit Igblastn expected input file name.

## vdjpuzzle conda environment

This environment is used to launch `VDJPUZZLE`, `BASIC`.
```
source /opt/anaconda/anaconda3/etc/profile.d/conda.sh
conda activate vdjpuzzle
```

## Genomes

Genomes and indexes required by some tools are embedded in this docker image.

* human genome and annotation :  **/GENOMES/STAR_GRCh38/**
* human transcipts : **/GENOMES/GRCh38**
* Bowtie index : **/GENOMES/BOWTIE2_INDEXES**

STAR index must be created or downloaded by yourself. Bellow is the command line to create it from the embedded genomes. 

Replace <ncores> by the number of cpus returned by the *lscpu* command.
```
cd /GENOMES/STAR_GRCh38
STAR --runThreadN <ncores> /
--runMode genomeGenerate /
--genomeDir STAR_GRCh38_index /
--genomeFastaFiles Homo_sapiens.GRCh38.dna.primary_assembly.fa /
--sjdbGTFfile Homo_sapiens.GRCh38.86.gtf /
--sjdbOverhang 100
```

## Sample
 A collection of sample scripts are available to test and benchmark software. Just go in each subdirectory and launch **./run.sh**. 

```
cd /opt/sample

```
to run `TRUST4` test just run:

```
run-trust4 -b /opt/TRUST4/example/example.bam -f /opt/TRUST4/hg38_bcrtcr.fa --ref /opt/TRUST4/human_IMGT+C.fa
``` 
