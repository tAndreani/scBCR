FROM debian:10.6

RUN apt update && apt install -y apt-transport-https ca-certificates wget dirmngr gnupg software-properties-common && wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add - && add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/  && apt update && apt install -y adoptopenjdk-8-hotspot

RUN apt-get update && apt-get -y upgrade
RUN apt-get -y install wget curl unzip build-essential zlib1g-dev git python3 python3-pip bowtie2
RUN cd /opt && wget https://github.com/trinityrnaseq/trinityrnaseq/archive/Trinity-v2.4.0.zip
RUN cd /opt && unzip Trinity-v2.4.0.zip && rm Trinity-v2.4.0.zip && mv /opt/trinityrnaseq-Trinity-v2.4.0 trinity
COPY patch/trinity/chrysalis/Makefile /opt/trinity/Chrysalis/
RUN cd /opt/trinity && make
RUN cd /opt && wget https://ftp.ncbi.nih.gov/blast/executables/igblast/release/1.4.0/ncbi-igblast-1.4.0-x64-linux.tar.gz
RUN cd /opt && tar -xzf  ncbi-igblast-1.4.0-x64-linux.tar.gz && rm  ncbi-igblast-1.4.0-x64-linux.tar.gz && mv ncbi-igblast-1.4.0 igblast
RUN cd /opt/igblast/bin/ && wget -R index.html --no-parent -r -l2 https://ftp.ncbi.nih.gov/blast/executables/igblast/release/old_internal_data/  && mv ftp.ncbi.nih.gov/blast/executables/igblast/release/old_internal_data/ ./internal_data  && rm -r ftp.ncbi.nih.gov
ENV IGDATA=/opt/igblast/bin
RUN cd /opt && wget https://github.com/alexdobin/STAR/archive/2.7.6a.tar.gz && tar -xzf 2.7.6a.tar.gz && mv STAR-2.7.6a STAR && cd /opt/STAR/source && make STAR && cp STAR ../bin/Linux_x86_64/ && rm /opt/2.7.6a.tar.gz

ENV PATH=/opt/STAR/bin/Linux_x86_64:$PATH

RUN mkdir -p /GENOMES/STAR_GRCh38
RUN cd /GENOMES/STAR_GRCh38 
#&& wget http://ftp.ensembl.org/pub/release-89/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz &#& wget http://ftp.ensembl.org/pub/release-89/gtf/homo_sapiens/Homo_sapiens.GRCh38.89.gtf.gz && cd /GENOMES/#STAR_GRCh38 && gunzip Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz && gunzip Homo_sapiens.GRCh38.89.gtf.gz

# BOWTIE2 INDEX
#
RUN mkdir -p /GENOMES/BOWTIE2_INDEXES
#RUN cd /GENOMES/BOWTIE2_INDEXES && bowtie2-build /GENOMES/STAR_GRCh38/Homo_sapiens.GRCh38.dna.primary_assembly.fa #homo_sapiens_GRCh38 && cp /GENOMES/STAR_GRCh38/Homo_sapiens.GRCh38.dna.primary_assembly.fa /GENOMES/BOWTIE2_INDEXES/#homo_sapiens_GRCh38.fa
ENV BOWTIE2_INDEXES=/GENOMES/BOWTIE2_INDEXES

RUN cd /opt && wget https://github.com/pachterlab/kallisto/releases/download/v0.43.1/kallisto_linux-v0.43.1.tar.gz
RUN cd /opt && tar -xzf kallisto_linux-v0.43.1.tar.gz && mv kallisto_linux-v0.43.1 kallisto && rm kallisto_linux-v0.43.1.tar.gz
RUN cd /opt && wget https://github.com/COMBINE-lab/salmon/releases/download/v0.8.2/Salmon-0.8.2_linux_x86_64.tar.gz
RUN cd /opt && tar -xzf Salmon-0.8.2_linux_x86_64.tar.gz && mv /opt/Salmon-0.8.2_linux_x86_64 /opt/salmon && rm Salmon-0.8.2_linux_x86_64.tar.gz
RUN apt-get -y install liblasi0 libgd3 libgts-0.7-5  libltdl7 freeglut3 libglade2-0 libglu1-mesa libglu1 libgtkglext1 libxaw7
RUN wget https://www2.graphviz.org/Archive/stable/ubuntu/ub13.10/x86_64/libgraphviz4_2.38.0-1~saucy_amd64.deb
RUN dpkg -i libgraphviz4_2.38.0-1~saucy_amd64.deb && apt-get -y -f install
RUN wget https://www2.graphviz.org/Archive/stable/ubuntu/ub13.10/x86_64/graphviz_2.38.0-1~saucy_amd64.deb
RUN dpkg -i graphviz_2.38.0-1~saucy_amd64.deb && apt-get -y -f install && rm libgraphviz4_2.38.0-1~saucy_amd64.deb && rm graphviz_2.38.0-1~saucy_amd64.deb
RUN cd /opt && wget https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.6.0/ncbi-blast-2.6.0+-x64-linux.tar.gz
RUN cd /opt && tar -xzf ncbi-blast-2.6.0+-x64-linux.tar.gz && mv ncbi-blast-2.6.0+ blast && rm ncbi-blast-2.6.0+-x64-linux.tar.gz
RUN cd /opt && wget http://evolution.gs.washington.edu/phylip/download/phylip-3.697.tar.gz
RUN cd /opt && tar -xzf phylip-3.697.tar.gz && rm phylip-3.697.tar.gz && mv phylip-3.697 phylip
RUN cd /opt/phylip/src && make -f Makefile.unx install
RUN cd /opt && wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.5.zip
RUN cd /opt && unzip fastqc_v0.11.5.zip && rm fastqc_v0.11.5.zip
RUN chmod 755 /opt/FastQC/fastqc
RUN ln -s /opt/FastQC/fastqc /usr/local/bin/fastqc
RUN cd /opt && curl -fsSL https://github.com/FelixKrueger/TrimGalore/archive/0.6.6.tar.gz -o trim_galore.tar.gz
RUN cd /opt && tar xvzf trim_galore.tar.gz && mv TrimGalore-0.6.6 trimgalore && rm trim_galore.tar.gz
RUN apt-get -y install r-base libxml2-dev
RUN R -e "install.packages(c('alakazam', 'ggplot2'), repos='http://cran.us.r-project.org')"
COPY bracer.tgz /opt
RUN cd /opt && tar xzf bracer.tgz
RUN pip3 install wheel
RUN apt-get install -y  isal libisal-dev libisal2 libcairo2 libcairo2-dev autoconf libtool nasm yasm
COPY patch/bracer/requirements_stable.txt /opt/BRACER/docker_helper_files/
RUN cd /opt/BRACER && python3 -m pip install -r docker_helper_files/requirements_stable.txt && python3 setup.py install

RUN mkdir /GENOMES/GRCm38 /GENOMES/GRCh38 -p

RUN cd /GENOMES/GRCh38 && wget -q https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_39/gencode.v39.transcripts.fa.gz && gunzip gencode.v39.transcripts.fa.gz && mv gencode.v39.transcripts.fa transcripts.fasta && python3 /opt/BRACER/docker_helper_files/gencode_parse.py transcripts.fasta
RUN cd /GENOMES/GRCm38 && wget -q https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_mouse/release_M28/gencode.vM28.transcripts.fa.gz && gunzip gencode.vM28.transcripts.fa.gz && mv gencode.vM28.transcripts.fa transcripts.fasta && python3 /opt/BRACER/docker_helper_files/gencode_parse.py transcripts.fasta


RUN cp /opt/BRACER/docker_helper_files/docker_bracer.conf ~/.bracerrc
RUN apt-get install -y samtools datamash
RUN pip3 install --upgrade pip
#RUN pip3 install caper
#RUN mkdir /root/.caper && caper init sge
RUN cd /opt  && git clone https://github.com/sdparekh/zUMIs.git
RUN cd /opt  && git clone https://github.com/liulab-dfci/TRUST4.git
RUN cd /opt  && curl -s -L -O https://github.com/milaboratory/mixcr/releases/download/v3.0.13/mixcr-3.0.13.zip && unzip mixcr-3.0.13.zip && mv mixcr-3.0.13 mixcr && rm mixcr-3.0.13.zip
RUN mkdir /opt/wdltool && cd /opt/wdltool && wget https://github.com/broadinstitute/wdltool/releases/download/0.14/wdltool-0.14.jar
RUN rm /opt/igblast/bin/internal_data -Rf
RUN cd /opt/igblast/bin/ && wget -e robots=off -R index.html --no-parent -r -l2 https://ftp.ncbi.nih.gov/blast/executables/igblast/release/old_internal_data/
RUN cd /opt/igblast/bin/ && mv ftp.ncbi.nih.gov/blast/executables/igblast/release/old_internal_data/ ./internal_data && rm -r ftp.ncbi.nih.gov
RUN apt-get install -y nano vim	





RUN cd /opt && wget https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh && chmod +x Anaconda3-2020.11-Linux-x86_64.sh && ./Anaconda3-2020.11-Linux-x86_64.sh -b -p /opt/anaconda/anaconda3/


# VDJPUZZLE + basic 
#
RUN cd /opt && git clone https://bitbucket.org/kirbyvisp/vdjpuzzle.git && mv vdjpuzzle vdjpuzzle2 && /opt/anaconda/anaconda3/bin/conda env create -f vdjpuzzle2/environment.yml
RUN /opt/anaconda/anaconda3/bin/conda install --name vdjpuzzle -c bioconda basic


# BALDR + BRAPeS
#
RUN cd /opt && wget https://github.com/trinityrnaseq/trinityrnaseq/archive/Trinity-v2.3.2.zip && unzip Trinity-v2.3.2.zip && cd trinityrnaseq-Trinity-v2.3.2
COPY patch/trinity/chrysalis/Makefile /opt/trinityrnaseq-Trinity-v2.3.2/Chrysalis/Makefile
RUN cd /opt/trinityrnaseq-Trinity-v2.3.2/ && make

RUN /opt/anaconda/anaconda3/bin/conda config --set channel_priority strict && /opt/anaconda/anaconda3/bin/conda create -n omics -c bioconda python pysam biopython seqtk=1.2 trimmomatic=0.32
RUN cd /opt && wget "https://github.com/BosingerLab/BALDR/archive/master.zip" -O baldr.zip && unzip baldr.zip && mv BALDR-master BALDR && rm baldr.zip
#RUN cd /opt && wget "https://github.com/YosefLab/BRAPeS/archive/master.zip" -O brapes.zip && unzip brapes.zip && mv BRAPeS-master BRAPES && rm brapes.zip
COPY patch/baldr/BALDR /opt/BALDR/BALDR
RUN chmod +x /opt/BALDR/BALDR


RUN cd /opt && wget https://ftp.ncbi.nih.gov/blast/executables/igblast/release/1.6.1/ncbi-igblast-1.6.1-x64-linux.tar.gz
RUN cd /opt && tar -xzf  ncbi-igblast-1.6.1-x64-linux.tar.gz && rm  ncbi-igblast-1.6.1-x64-linux.tar.gz && mv ncbi-igblast-1.6.1 igblast_1.6.1
RUN cd /opt/igblast_1.6.1/bin/ && wget -R index.html --no-parent -r -l2 https://ftp.ncbi.nih.gov/blast/executables/igblast/release/old_internal_data/  && mv ftp.ncbi.nih.gov/blast/executables/igblast/release/old_internal_data/ ./internal_data  && rm -r ftp.ncbi.nih.gov

RUN cd /opt && git clone https://github.com/deweylab/RSEM.git &&  cd RSEM && make && make install prefix=/opt/rsem

# PATH
#
ENV PATH=/usr/bin:/opt/rsem/bin:/opt/BRACER:/opt/blast/bin:/opt/igblast/bin:/opt/salmon/bin:/opt/STAR/bin/Linux_x86_64/:/opt/kallisto:/opt/phylip/exe:/opt/trinity:/opt/BALDR:/opt/vdjpuzzle2/bin:/opt/zUMIs:/opt/mixcr:/opt/STAR/bin/Linux_x86_64/:/opt/anaconda/anaconda3/bin:$PATH
#ENV PATH=/usr/bin:/opt/rsem/bin:/opt/BRACER:/opt/blast/bin:/opt/igblast/bin:/opt/salmon/bin:/opt/STAR/bin/Linux_x86_64/:/opt/kallisto:/opt/phylip/exe:/opt/trinity:/opt/BALDR:/opt/vdjpuzzle2/bin:/opt/BRAPES:/opt/zUMIs:/opt/mixcr:/opt/STAR/bin/Linux_x86_64/:/opt/anaconda/anaconda3/bin:$PATH
ENV CLASSPATH=/wdltool
ENV SOFT_IGBLAST_1.6.1=/opt/igblast_1.6.1/bin
ENV SOFT_IGBLAST_1.4=/opt/igblast/bin

# SAMPLES
#
COPY sample /opt/sample


# OS PREF
#
COPY bashrc /root/.bashrc
COPY bashrc /etc/bashrc
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.7 1
