#The path to the tools Trinity, Trimmomatic etc.. should be within the anaconda image, if not please let me know

source /opt/anaconda/anaconda3/etc/profile.d/conda.sh
conda activate omics

BALDR --single A10_S73_human_L007_R1_001.fastq.gz --trinity /opt/trinityrnaseq-Trinity-v2.3.2/Trinity --adapter /opt/anaconda/anaconda3/envs/omics/share/trimmomatic-0.32-3/adapters/NexteraPE-PE.fa --trimmomatic /opt/anaconda/anaconda3/envs/omics/share/trimmomatic-0.32-3/trimmomatic.jar --igblastn /opt/igblast_1.6.1/bin/igblastn --STAR STAR --STAR_index /GENOMES/STAR_GRCh38/STAR_GRCh38_index --BALDR /opt/BALDR/ --memory 40G --threads 6
