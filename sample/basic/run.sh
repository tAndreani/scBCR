#How to run Basic, during the installation you should have also have downloaded the annotation files where BAISC can find them in the parameters: BCR and human

source /opt/anaconda/anaconda3/etc/profile.d/conda.sh
conda activate vdjpuzzle

mkdir test/04.tmp;
BASIC.py -b /opt/anaconda/anaconda3/envs/vdjpuzzle2/bin -PE_1 99.IGHC_IGHV.IGKC_IGKV.R1.fq.gz  -PE_2 99.IGHC_IGHV.IGKC_IGKV.R2.fq.gz  -g human -i BCR -o test/ -t test/04.tmp -n 04
