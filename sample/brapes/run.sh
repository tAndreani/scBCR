## info to run it https://github.com/YosefLab/BRAPeS/blob/master/README.md

source /opt/anaconda/anaconda3/etc/profile.d/conda.sh
conda activate omics

brapes.py -genome data/mm10_ncbi -path proc_data/ -sumF BRAPeS_out/BCR.out -output BRAPeS_out/BCR.out -score 15 -top 10 -byExp -unmapped unmapped.bam -bam sorted.bam
