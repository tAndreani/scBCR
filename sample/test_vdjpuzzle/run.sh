## How to run bvdjpuzzle: Example is the folder with the two fastq files, the others should be installed with the software. If not please let me know

source /opt/anaconda/anaconda3/etc/profile.d/conda.sh
conda activate vdjpuzzle2

vdjpuzzle Example --bowtie-index=/GENOMES/BOWTIE2_INDEXES/homo_sapiens_GRCh38 --gtf=/GENOMES/STAR_GRCh38/Homo_sapiens.GRCh38.86.gtf --type=b --CPU=2  
